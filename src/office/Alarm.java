package office;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;

import office.StopwatchUI.MenuActionListener;

/**
 * Alarm.
 * @author Kitipoom Kongpetch
 *
 */

public class Alarm extends JFrame {
	private JTextField timescreen;
	private int hour, min, sec, sethour, setmin, setsec;
	private Calendar rightNow;
	private SwitchState turnstate = SwitchState.OFF;
	private SettingState setstate = SettingState.HOUR;
	private JButton set, turn, plus, minus;
	private AudioInputStream soundSteam = null;
	private Clip clip;
	private SettingTimeStateplus plust = new SettingTimeStateplus();
	private SettingTimeStateminus minust = new SettingTimeStateminus();
	private Color textcolor= Color.YELLOW;
	private Color alarmtextcolor= Color.GREEN;
	private JMenu menu;
	private JMenuBar menubar;
	/**
	 * Set JFrame.
	 */
	public Alarm() {
		setTitle("Alarm");
		setResizable(false);
		set = new JButton("set");
		turn = new JButton("OFF");
		plus = new JButton("+");
		minus = new JButton("-");
		timescreen = new JTextField(10);
		timescreen.setHorizontalAlignment(JTextField.CENTER);
		timescreen.setForeground(Color.YELLOW);
		timescreen.setBackground(Color.BLACK);
		JPanel p = new JPanel();
		timescreen.setEditable(false);
		timescreen.setFont(new Font("Arial", Font.PLAIN, 60));

		try {
			sound();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// sound();
		menubar = new JMenuBar();
		menu = new JMenu("Color");
		Colortype[] ct = Colortype.values();
		for (int i = 0; i < ct.length; i++) {
			JMenuItem item = new JMenuItem(ct[i].toString());
			item.addActionListener(new MenuActionListener());
			menu.add(item);
		}
		menu.getSelectedIcon();
		menubar.add(menu);
		Container cp = getContentPane();
		BoxLayout layout = new BoxLayout(cp, BoxLayout.Y_AXIS);
		cp.setLayout(layout);
		cp.add(timescreen);
		p.add(menubar);
		p.add(turn);
		p.add(set);
		p.add(plus);
		p.add(minus);
		cp.add(p);
		Timer t = new Timer(500, new Listener());
		t.start();
		ActionListener turnalarm = new Turnalarm();
		turn.addActionListener(turnalarm);
		ActionListener settime = new Setalarm();
		set.addActionListener(settime);
		ActionListener setplus = new Plus();
		plus.addActionListener(setplus);
		ActionListener setminus = new Minus();
		minus.addActionListener(setminus);
		timescreen.setText(sethour + ": " + setmin + ": " + setsec);
	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			rightNow = Calendar.getInstance();
			hour = rightNow.get(Calendar.HOUR_OF_DAY);
			min = rightNow.get(Calendar.MINUTE);
			sec = rightNow.get(Calendar.SECOND);
			if (turnstate == SwitchState.ON) {
				if (hour == sethour && min == setmin && sec == setsec) {
					timescreen.setForeground(alarmtextcolor);
					clip.start();
					clip.loop(Clip.LOOP_CONTINUOUSLY);
				}
			}
		}

	}

	class Turnalarm implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (turnstate == SwitchState.OFF) {
				turnstate = SwitchState.ON;
				setstate = SettingState.NON;
				turn.setText("ON");
			} else if (turnstate == SwitchState.ON) {
				turnstate = SwitchState.OFF;
				setstate = SettingState.HOUR;
				clip.stop();
				turn.setText("OFF");
				timescreen.setForeground(textcolor);
			}

		}

	}

	class Setalarm implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (setstate == SettingState.HOUR) {
				setstate = SettingState.MIN;
			} else if (setstate == SettingState.MIN) {
				setstate = SettingState.SEC;
			} else if (setstate == SettingState.SEC) {
				setstate = SettingState.HOUR;
			}
		}

	}

	class Plus implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			setClock(plust);
		}

	}

	class Minus implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			setClock(minust);
		}

	}

	/**
	 * Make sound.
	 * @throws IOException
	 * @throws LineUnavailableException
	 * @throws LineUnavailableException .
	 * @throws IOException .
	 */
	public void sound() throws LineUnavailableException, IOException {
		URL audio = ClassLoader.getSystemResource(("Music/alarm_beep.wav"));
		try {
			soundSteam = AudioSystem.getAudioInputStream(audio);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		clip.open(soundSteam);
	}
	/**
	 * Set time.
	 * @param cstate .
	 */
	public void setClock(ClockState cstate) {
		if (setstate == SettingState.HOUR) {
			sethour = cstate.performSet(sethour, setstate);
		} else if (setstate == SettingState.MIN) {
			setmin = cstate.performSet(setmin, setstate);
		} else if (setstate == SettingState.SEC) {
			setsec = cstate.performSet(setsec, setstate);
		}
		timescreen.setText(cstate.updateTime(sethour, setmin, setsec));
	}
	class MenuActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Red")) {
				timescreen.setBackground(Color.red);
				alarmtextcolor = Color.YELLOW;
				textcolor = Color.BLUE;
				timescreen.setForeground(Color.blue);
				
			} else if (e.getActionCommand().equals("Blue")) {
				timescreen.setBackground(Color.BLUE);
				alarmtextcolor = Color.YELLOW;
				textcolor = Color.RED;
				timescreen.setForeground(Color.RED);
			} else if (e.getActionCommand().equals("Green")) {
				timescreen.setBackground(Color.green);
				alarmtextcolor = Color.RED;
				textcolor = Color.YELLOW;
				timescreen.setForeground(Color.YELLOW);
			} else if (e.getActionCommand().equals("Black")){
				timescreen.setBackground(Color.BLACK);
				alarmtextcolor = Color.WHITE;
				textcolor = Color.YELLOW;
				timescreen.setForeground(Color.YELLOW);
			} 
			else if(e.getActionCommand().equals("White")){
				timescreen.setBackground(Color.WHITE);
				alarmtextcolor = Color.RED;
				textcolor = Color.BLACK;
				timescreen.setForeground(Color.BLACK);
			}
		}

	}

	/**
	 * Main.
	 * @param args
	 *            .
	 * @throws LineUnavailableException .
	 * @throws IOException .
	 */
	public static void main(String[] args) throws LineUnavailableException,
			IOException {
		run(new Alarm(), 300, 150);
		Alarm clock = new Alarm();
	}

	/**
	 * Run program.
	 * 
	 * @param frame
	 *            program
	 * @param width
	 *            width
	 * @param height
	 *            height
	 */
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
}
