package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public enum Area implements Unit {
	METER("meter", 1.0), CENTRIMETER("centrimeter", 0.01), KILOMETER(
			"kilometer", 1000.0), MILE("mile", 1609.344), FOOT("foot", 0.30480), WA(
			"wa", 2.0);
	public final double value;
	public final String name;

	private Area(String name, double value) {
		this.value = value;
		this.name = name;
	}
	/**
	 * 
	 * @param a name of type
	 * @return Unit
	 */
	public Area check(String a) {
		if (a.equals("meter")) {
			return Area.METER;
		}
		if (a.equals("centrimeter")) {
			return Area.CENTRIMETER;
		}
		if (a.equals("kilometer")) {
			return Area.KILOMETER;
		}
		if (a.equals("mile")) {
			return Area.MILE;
		}
		if (a.equals("foot")) {
			return Area.FOOT;
		}
		if (a.equals("wa")) {
			return Area.WA;
		}
		return null;
	}

	@Override
	public double convertTo(Unit u, double amt) {

		Area base = check(this.name);
		Area sub = check(u.toString());
		double result = amt * Math.pow(base.getValue(),2) / Math.pow(sub.getValue(),2);
		return result;
	}

	@Override
	public double getValue() {

		return this.value;
	}

	@Override
	public String toString() {
		return this.name;

	}
}

