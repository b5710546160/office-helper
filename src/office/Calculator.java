package office;

import javax.swing.JFrame;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class Calculator extends JFrame {

	private JLabel num1Label, num2Label, resultLabel;
	private JTextField num1TextField, num2TextField, resultTextField;
	private JButton addB, subtractB, multiplyB, divideB, clearB, percentageB,powB,moreB;
	private double num1, num2;

	/**
	 * Calculator.
	 */
	public Calculator() {
		setTitle("Calculator");
		setResizable(false);
		Container pane = getContentPane();
		GridLayout aGrid = new GridLayout(7, 2);
		pane.setLayout(aGrid);
		num1Label = new JLabel("1st Number");
		num2Label = new JLabel("2nd Number");
		resultLabel = new JLabel("Result");

		num1TextField = new JTextField(10);
		num2TextField = new JTextField(10);
		resultTextField = new JTextField(10);
		resultTextField.setEditable(false);

		addB = new JButton("+");
		subtractB = new JButton("-");
		multiplyB = new JButton("*");
		divideB = new JButton("/");
		clearB = new JButton("C");
		percentageB = new JButton("%");
		powB =new JButton("^");
		moreB=new JButton("M");

		pane.add(num1Label);
		pane.add(num1TextField);

		pane.add(num2Label);
		pane.add(num2TextField);

		pane.add(addB);
		pane.add(subtractB);
		pane.add(multiplyB);
		pane.add(divideB);
		pane.add(percentageB);
		pane.add(powB);
		pane.add(clearB);
		pane.add(moreB);

		pane.add(resultLabel);
		pane.add(resultTextField);

		AddButtonHandler addbHandler = new AddButtonHandler();
		addB.addActionListener(addbHandler);

		SubtractButtonHandler subtractbHandler = new SubtractButtonHandler();
		subtractB.addActionListener(subtractbHandler);

		MultiplyButtonHandler multiplybHandler = new MultiplyButtonHandler();
		multiplyB.addActionListener(multiplybHandler);

		PercentButtonHandler percenbHandler = new PercentButtonHandler();
		percentageB.addActionListener(percenbHandler);
		
		PowerButtonHandler powerHandler = new PowerButtonHandler();
		powB.addActionListener(powerHandler);

		ClearButtonHandler clearHandler = new ClearButtonHandler();
		clearB.addActionListener(clearHandler);
		Moreopen moreHandler = new Moreopen();
		moreB.addActionListener(moreHandler);

		divideB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String num1Str = num1TextField.getText();
				String num2Str = num2TextField.getText();
				if (num1Str.length() > 0 && num2Str.length() > 0) {
					setNumber(num1Str,num2Str);
					if (num2 == 0) {
						JOptionPane.showMessageDialog(null, "Division by 0");
					}
					else{
						double result = num1 / num2;
						String resultStr = Double.toString(result);
						resultTextField.setText(resultStr);
					}
				}
			}

		});

	}

	private class AddButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			if (num1Str.length() > 0 && num2Str.length() > 0) {
				setNumber(num1Str,num2Str);
				double result = num1 + num2;
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);
			}
		}
	}

	private class SubtractButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			if (num1Str.length() > 0 && num2Str.length() > 0) {
				setNumber(num1Str,num2Str);
				double result = num1 - num2;
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);
			}
		}
	}

	private class MultiplyButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			if (num1Str.length() > 0 && num2Str.length() > 0) {
				setNumber(num1Str,num2Str);
				double result = num1 * num2;
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);

			}
		}
	}

	private class PercentButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			if (num1Str.length() > 0 && num2Str.length() > 0) {
				setNumber(num1Str,num2Str);
				double result = num1 % num2;
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);

			}
		}
	}
	private class PowerButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			if (num1Str.length() > 0 && num2Str.length() > 0) {
				setNumber(num1Str,num2Str);
				double result = Math.pow(num1,num2);
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);

			}
		}
	}

	private class ClearButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			num1TextField.setText("");
			num2TextField.setText("");
			resultTextField.setText("");

		}
	}
	class Moreopen implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Morecalculator mcals = new Morecalculator();
			mcals.run(mcals,250, 250);
			
		}
		
	}
	/**
	 * 
	 * @param num1Str First input
	 * @param num2Str Second input
	 */
	private void setNumber(String num1Str,String num2Str){
		num1 = Double.parseDouble(num1Str);
		num2 = Double.parseDouble(num2Str);
	}
	/**
	 * 
	 * @param frame .
	 * @param width .
	 * @param height .
	 */
	
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
	/**
	 * 
	 * @param args .
	 */
	public static void main(String[] args) {
		Calculator aCalculatorGui = new Calculator();
		aCalculatorGui.run(aCalculatorGui, 250, 250);

	}
}
