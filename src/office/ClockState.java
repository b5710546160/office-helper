package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public abstract class ClockState {
	/**
	 * 
	 */
	public ClockState() {
	}

	/** handle "set" key press. 
	 * @param time to set
	 * @param state now state
	 * @return new time
	 * */
	public abstract int performSet(int time, SettingState state);

	/** handle updateTime event notification. 
	 * @param hour .
	 * @param min .
	 * @param sec .
	 * @return time.
	 * */
	public abstract String updateTime(int hour, int min, int sec);

	/** something to do before exiting this state. */
}
