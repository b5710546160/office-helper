package office;
/**
 * List color for select to use.
 * @author Kitipoom Kongpetch
 *
 */
public enum Colortype {
	Red("red"),
	Blue("blue"),
	Green("green"),
	Black("black"),
	White("white");
	private String color;
	private Colortype(String color){
		this.color=color;
	}

}
