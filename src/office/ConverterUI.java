package office;

import javax.swing.*;
import javax.swing.event.AncestorListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class ConverterUI extends JFrame {
	// attributes for graphical components
	private UnitConverter uc = new UnitConverter();
	private JMenu menu;
	private JMenuBar menubar;
	private JButton convertButton,reverseconvertButton, clearButton;
	private UnitConverter unitconverter;
	private JTextField inputField2;
	private JTextField inputField1;
	private JComboBox<Unit> combobox1;
	private JComboBox<Unit> combobox2;
	private JComboBox<Unit> combobox1L, combobox2L;
	private JComboBox<Unit> combobox1A, combobox2A;
	private JComboBox<Unit> combobox1W, combobox2W;
	private JComboBox<Unit> combobox1C, combobox2C;
	private String control = "Length";

	/**
	 * 
	 *UnitConverter.
	 */
	public ConverterUI() {
		this.unitconverter = uc;
		this.setTitle("Converter");
		setSize(200, 200);
		initComponents();
		this.pack();
	}

	/**
	 * initialize components in the window.
	 */
	private void initComponents() {

		// manager. Create event listeners and add them to components.

		Container contents = this.getContentPane();
		LayoutManager layout = new BoxLayout(contents, BoxLayout.Y_AXIS);
		contents.setLayout(layout);
		convertButton = new JButton("Convert");
		reverseconvertButton = new JButton("Reverse");
		clearButton = new JButton("Clear");
		inputField2 = new JTextField(10);
		inputField1 = new JTextField(10);
		menubar = new JMenuBar();
		menu = new JMenu("File");
		UnitType[] utype = UnitType.values();
		for (int i = 0; i < utype.length; i++) {
			JMenuItem item = new JMenuItem(utype[i].toString());
			item.addActionListener(new MenuActionListener());
			menu.add(item);
		}
		JMenuItem item2 = new JMenuItem("Edit...");
		item2.addActionListener(new MenuActionListener());
		menu.add(item2);
		menu.getSelectedIcon();
		menubar.add(menu);

		combobox1 = new JComboBox();
		combobox2 = new JComboBox();
		combobox1L = new JComboBox(unitconverter.getUnits("Length"));
		combobox2L = new JComboBox(unitconverter.getUnits("Length"));
		combobox1A = new JComboBox(unitconverter.getUnits("Area"));
		combobox1W = new JComboBox(unitconverter.getUnits("Weight"));
		combobox1C = new JComboBox(unitconverter.getUnits("Cube"));
		combobox2A = new JComboBox(unitconverter.getUnits("Area"));
		combobox2W = new JComboBox(unitconverter.getUnits("Weight"));
		combobox2C = new JComboBox(unitconverter.getUnits("Cube"));
		//
		combobox1A.setVisible(false);
		combobox1L.setVisible(true);
		combobox1W.setVisible(false);
		combobox1C.setVisible(false);
		combobox2A.setVisible(false);
		combobox2L.setVisible(true);
		combobox2W.setVisible(false);
		combobox2C.setVisible(false);
		//
		Container contents1 = this.getContentPane();
		LayoutManager layout1 = new GridLayout();
		JPanel p = new JPanel();
		contents.setLayout(layout);
		p.add(inputField1);
		p.add(combobox1L);
		p.add(combobox1A);
		p.add(combobox1W);
		p.add(combobox1C);
		contents.add(p);
		p = new JPanel();
		p.add(inputField2);
		p.add(combobox2L);
		p.add(combobox2A);
		p.add(combobox2W);
		p.add(combobox2C);
		contents.add(p);
		p = new JPanel();
		p.add(menubar);
		p.add(convertButton);
		p.add(reverseconvertButton);
		p.add(clearButton);
		contents.add(p);
		ActionListener listener = new ConvertButtonListener();
		convertButton.addActionListener(listener);
		ActionListener listener1 = new ClearButtonListener();
		clearButton.addActionListener(listener1);
		inputField1.addActionListener(listener);
		ActionListener listener2 =new ReverseconvertButtonListener();
		inputField2.addActionListener(listener2);
		reverseconvertButton.addActionListener(listener2);
	}

	/**
	 * 
	 * @param str
	 *            String
	 * @return String is number or not
	 */
	public boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed. */
		public void actionPerformed(ActionEvent evt) {
			String s = inputField1.getText().trim();
			String f = inputField2.getText().trim();
			if (s.length() > 0) {
				double value = 0;
				if (isNumeric(s)) {
					inputField1.setForeground(Color.black);
					inputField2.setForeground(Color.black);
					value = Double.valueOf(s);
					double result = checkConvert(value);
					String resultf = String.format("%.5g", result);
					inputField2.setText(resultf);
				} else {
					JOptionPane.showMessageDialog(ConverterUI.this, "Invalid");
					inputField1.setForeground(Color.red);
				}

			}
			else if(f.length() > 0){
				double value = 0;
				if (isNumeric(f)) {
					inputField1.setForeground(Color.black);
					inputField2.setForeground(Color.black);
					value = Double.valueOf(f);
					double result = reversecheckConvert(value);
					String resultf = String.format("%.5g", result);
					inputField1.setText(resultf);
				} else {
					JOptionPane.showMessageDialog(ConverterUI.this, "Invalid");
					inputField2.setForeground(Color.red);
				}
			}

		}
	}
	class ReverseconvertButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String f = inputField2.getText().trim();
			if(f.length() > 0){
				double value = 0;
				if (isNumeric(f)) {
					inputField2.setForeground(Color.black);
					value = Double.valueOf(f);
					double result = reversecheckConvert(value);
					String resultf = String.format("%.5g", result);
					inputField1.setText(resultf);
				} else {
					JOptionPane.showMessageDialog(ConverterUI.this, "Invalid");
					inputField2.setForeground(Color.red);
				}
			}
		}
		
	}
	/**
	 * 
	 * @param base Value is we want to change
	 * @return Value converted
	 */
	public double checkConvert(double base) {
		double result = 0;
		if (control.equals("Length")) {
			Length unit1 = (Length) combobox1L.getSelectedItem();
			Length unit2 = (Length) combobox2L.getSelectedItem();
			result = unitconverter.convert(base, unit1, unit2);
		} else if (control.equals("Area")) {
			Area unit1 = (Area) combobox1A.getSelectedItem();
			Area unit2 = (Area) combobox2A.getSelectedItem();
			result = unitconverter.convert(base, unit1, unit2);
		} else if (control.equals("Weight")) {
			Weight unit1 = (Weight) combobox1W.getSelectedItem();
			Weight unit2 = (Weight) combobox2W.getSelectedItem();
			result = unitconverter.convert(base, unit1, unit2);
		} else {
			Cube unit1 = (Cube) combobox1C.getSelectedItem();
			Cube unit2 = (Cube) combobox2C.getSelectedItem();
			result = unitconverter.convert(base, unit1, unit2);
		}
		return result;

	}
	/**
	 * 
	 * @param base Value is we want to change
	 * @return Value converted
	 */
	public double reversecheckConvert(double base) {
		double result = 0;
		if (control.equals("Length")) {
			Length unit1 = (Length) combobox1L.getSelectedItem();
			Length unit2 = (Length) combobox2L.getSelectedItem();
			result = unitconverter.convert(base, unit2, unit1);
		} else if (control.equals("Area")) {
			Area unit1 = (Area) combobox1A.getSelectedItem();
			Area unit2 = (Area) combobox2A.getSelectedItem();
			result = unitconverter.convert(base, unit2, unit1);
		} else if (control.equals("Weight")) {
			Weight unit1 = (Weight) combobox1W.getSelectedItem();
			Weight unit2 = (Weight) combobox2W.getSelectedItem();
			result = unitconverter.convert(base, unit2, unit1);
		} else {
			Cube unit1 = (Cube) combobox1C.getSelectedItem();
			Cube unit2 = (Cube) combobox2C.getSelectedItem();
			result = unitconverter.convert(base, unit2, unit1);
		}
		return result;

	}

	class ClearButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			inputField2.setText(" ");
			inputField1.setText(" ");
			combobox1.removeAll();
			combobox2.removeAll();

		}

	}

	class MenuActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			control = e.getActionCommand();
			if (e.getActionCommand().equals("Area")) {

				combobox1A.setVisible(true);
				combobox1L.setVisible(false);
				combobox1W.setVisible(false);
				combobox1C.setVisible(false);
				combobox2A.setVisible(true);
				combobox2L.setVisible(false);
				combobox2W.setVisible(false);
				combobox2C.setVisible(false);
			} else if (e.getActionCommand().equals("Length")) {

				combobox1A.setVisible(false);
				combobox1L.setVisible(true);
				combobox1W.setVisible(false);
				combobox1C.setVisible(false);
				combobox2A.setVisible(false);
				combobox2L.setVisible(true);
				combobox2W.setVisible(false);
				combobox2C.setVisible(false);
			} else if (e.getActionCommand().equals("Weight")) {
				combobox1A.setVisible(false);
				combobox1L.setVisible(false);
				combobox1W.setVisible(true);
				combobox1C.setVisible(false);
				combobox2A.setVisible(false);
				combobox2L.setVisible(false);
				combobox2W.setVisible(true);
				combobox2C.setVisible(false);
			} else if (e.getActionCommand().equals("Cube")){
				combobox1A.setVisible(false);
				combobox1L.setVisible(false);
				combobox1W.setVisible(false);
				combobox1C.setVisible(true);
				combobox2A.setVisible(false);
				combobox2L.setVisible(false);
				combobox2W.setVisible(false);
				combobox2C.setVisible(true);
			} 
			else if(e.getActionCommand().equals("Edit...")){
				EditconverterUI convert = new EditconverterUI();
				convert.run(convert,300,180);
			}
		}

	}



	/**
	 * 
	 * @param args
	 * runprogram
	 */
	public static void main(String[] args) {
		JFrame convert = new ConverterUI();
		run(convert,300,150);
	}
	/**
	 * Run program.
	 * 
	 * @param frame
	 *            program
	 * @param width
	 *            width
	 * @param height
	 *            height
	 */
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
}
