package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public enum Cube implements Unit {
	METER("meter", 1.0), CENTRIMETER("centrimeter", 0.01), KILOMETER(
			"kilometer", 1000.0), MILE("mile", 1609.344), FOOT("foot", 0.30480), WA(
			"wa", 2.0);
	public final double value;
	public final String name;

	private Cube(String name, double value) {
		this.value = value;
		this.name = name;
	}
	/**
	 * 
	 * @param a name of type
	 * @return Unit
	 */
	public Cube check(String a) {
		if (a.equals("meter")) {
			return Cube.METER;
		}
		if (a.equals("centrimeter")) {
			return Cube.CENTRIMETER;
		}
		if (a.equals("kilometer")) {
			return Cube.KILOMETER;
		}
		if (a.equals("mile")) {
			return Cube.MILE;
		}
		if (a.equals("foot")) {
			return Cube.FOOT;
		}
		if (a.equals("wa")) {
			return Cube.WA;
		}
		return null;
	}

	@Override
	public double convertTo(Unit u, double amt) {

		Cube base = check(this.name);
		Cube sub = check(u.toString());
		double result = amt * Math.pow(base.getValue(),3) / Math.pow(sub.getValue(),3);
		return result;
	}

	@Override
	public double getValue() {

		return this.value;
	}

	@Override
	public String toString() {
		return this.name;

	}
}
