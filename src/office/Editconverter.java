package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class Editconverter {
	/**
	 * 
	 * @param base value input
	 * @param convertunit converter for change value input
	 * @return result
	 */
	public double convert(double base, double convertunit){
		return base*convertunit;
	}
	/**
	 * 
	 * @param base value input
	 * @param convertunit converter for change value input
	 * @return result
	 */
	public double reverseconvert(double base, double convertunit){
		return base/convertunit;
	}
}
