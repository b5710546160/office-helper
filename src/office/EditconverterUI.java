package office;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import office.ConverterUI.ClearButtonListener;
import office.ConverterUI.ConvertButtonListener;
import office.ConverterUI.MenuActionListener;
import office.ConverterUI.ReverseconvertButtonListener;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class EditconverterUI extends JFrame{
	private Editconverter uc = new Editconverter();
	private JButton convertButton,reverseconvertButton, clearButton;
	private Editconverter unitconverter;
	private JTextField inputField2;
	private JTextField inputField1;
	private JTextField  inputconvert;
	private JLabel con,input1,input2;

	/**
	 * 
	 * UnitConverter.
	 */
	public EditconverterUI() {
		this.unitconverter = uc;
		this.setTitle("Converter");
		setSize(200, 200);
		initComponents();
		this.pack();
	}

	/**
	 * initialize components in the window.
	 */
	private void initComponents() {
		Container contents = this.getContentPane();
		LayoutManager layout = new BoxLayout(contents, BoxLayout.Y_AXIS);
		contents.setLayout(layout);
		convertButton = new JButton("Convert");
		reverseconvertButton = new JButton("Reverse");
		clearButton = new JButton("Clear");
		inputField2 = new JTextField(10);
		inputField1 = new JTextField(10);
		inputconvert= new JTextField(10);
		input1 = new JLabel("1.");
		input2 = new JLabel("2.");
		con = new JLabel("X");
		Container contents1 = this.getContentPane();
		LayoutManager layout1 = new GridLayout();
		JPanel p = new JPanel();
		contents.setLayout(layout);
		p.add(input1);
		p.add(inputField1);
		contents.add(p);
		p = new JPanel();
		p.add(con);
		p.add(inputconvert);
		contents.add(p);
		p = new JPanel();
		p.add(input2);
		p.add(inputField2);
		contents.add(p);
		p = new JPanel();
		p.add(convertButton);
		p.add(reverseconvertButton);
		p.add(clearButton);
		contents.add(p);
		ActionListener listener = new ConvertButtonListener();
		convertButton.addActionListener(listener);
		ActionListener listener1 = new ClearButtonListener();
		clearButton.addActionListener(listener1);
		inputField1.addActionListener(listener);
		ActionListener listener2 =new ReverseconvertButtonListener();
		inputField2.addActionListener(listener2);
		reverseconvertButton.addActionListener(listener2);
	}

	/**
	 * 
	 * @param str
	 *            String
	 * @return String is number or not
	 */
	public boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed. */
		public void actionPerformed(ActionEvent evt) {
			String s = inputField1.getText().trim();
			String c = inputconvert.getText().trim();
			String f = inputField2.getText().trim();
			if (s.length() > 0&&c.length()>0) { 
				double value = 0;
				double basecon=0;
				if (isNumeric(s)&&isNumeric(c)) {
					inputField1.setForeground(Color.black);
					inputField2.setForeground(Color.black);
					inputconvert.setForeground(Color.black);
					value = Double.valueOf(s);
					basecon=Double.valueOf(c);
					double result = checkConvert(value,basecon);
					String resultf = String.format("%.5g", result);
					inputField2.setText(resultf);
				} else {
					JOptionPane.showMessageDialog(EditconverterUI.this, "Invalid");
					inputField1.setForeground(Color.red);
					inputconvert.setForeground(Color.red);
				}

			}
			else if(f.length() > 0&&c.length()>0){
				double value = 0;
				double basecon=0;
				if (isNumeric(f)) {
					inputField1.setForeground(Color.black);
					inputField2.setForeground(Color.black);
					inputconvert.setForeground(Color.black);
					value = Double.valueOf(f);
					basecon=Double.valueOf(c);
					double result = reversecheckConvert(value,basecon);
					String resultf = String.format("%.5g", result);
					inputField1.setText(resultf);
				} else {
					JOptionPane.showMessageDialog(EditconverterUI.this, "Invalid");
					inputField2.setForeground(Color.red);
					inputconvert.setForeground(Color.red);
				}
			}

		}
	}
	class ReverseconvertButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String f = inputField2.getText().trim();
			String c = inputconvert.getText().trim();
			if(f.length() > 0&&c.length()>0){
				double value = 0;
				double basecon=0;
				if (isNumeric(f)&&isNumeric(c)) {
					inputField2.setForeground(Color.black);
					inputconvert.setForeground(Color.black);
					value = Double.valueOf(f);
					basecon=Double.valueOf(c);
					double result = reversecheckConvert(value,basecon);
					String resultf = String.format("%.5g", result);
					inputField1.setText(resultf);
				} else {
					JOptionPane.showMessageDialog(EditconverterUI.this, "Invalid");
					inputField2.setForeground(Color.red);
					inputconvert.setForeground(Color.red);
				}
			}
		}
		
	}
	/**
	 * 
	 * @param base Value is we want to change
	 * @param converter use for convert
	 * @return Value converted
	 */
	public double checkConvert(double base,double converter) {
		double result = 0;
		result=this.unitconverter.convert(base, converter);
		return result;

	}
	/**
	 * 
	 * @param base Value is we want to change
	 * @param converter use for convert
	 * @return Value converted
	 */
	public double reversecheckConvert(double base,double converter) {
		double result = 0;
		result=this.unitconverter.reverseconvert(base, converter);
		return result;

	}

	class ClearButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			inputField2.setText("");
			inputField1.setText("");
			inputconvert.setText("");

		}

	}

	



	/**
	 * 
	 * @param args
	 * runprogram
	 */
	public static void main(String[] args) {
		JFrame convert = new EditconverterUI();
		run(convert,300,180);
	}
	/**
	 * Run program.
	 * 
	 * @param frame
	 *            program
	 * @param width
	 *            width
	 * @param height
	 *            height
	 */
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
}
