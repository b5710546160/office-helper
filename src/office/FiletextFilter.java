package office;
import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class FiletextFilter extends FileFilter {
	/**
	 * Check type file.
	 * @param f file
	 * @return true or false
	 */
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = Utils.getExtension(f);
        if (extension != null) {
        	return extension.equals(Utils.WAV) ? true : false;
        }

        return false;
    }

    /**
     * The description of this filter.
     * @return Description
     */
    public String getDescription() {
        return "Music(.wav)";
    }
}