package office;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;

import javax.imageio.ImageIO;
import javax.sound.sampled.Clip;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import office.Alarm.Listener;
/**
 * First page. 
 * @author Kitipoom Kongpetch
 *
 */
public class HelperInterface extends JFrame{
	private JButton buttonOpen;
	private Calendar rightNow;
	private ImageIcon iconOpen ;
	private ClassLoader loader = this.getClass().getClassLoader();
	private URL awaking,sleeping,awaking2,sleeping2,awaking3,sleeping3;
	private ImageIcon imageIconday,imageIconnight,imageIconday2,imageIconnight2,imageIconday3,imageIconnight3;
	private int hour, min, sec, sethour, setmin, setsec;
	/**
	 * Set interface.
	 */
	public HelperInterface(){
		this.setTitle("Helper");
		//setResizable(false);
		setAlwaysOnTop( true );
		buttonOpen =new JButton("");
		buttonOpen.setContentAreaFilled(false);
		loadPicture();
		Timer t = new Timer(500, new Listener());
		t.start();
		Container cp = getContentPane();
		BoxLayout layout = new BoxLayout(cp, BoxLayout.Y_AXIS);
		cp.setLayout(layout);
		cp.add(buttonOpen);
		buttonOpen.addActionListener(new Mainopen());
	}
	class Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			rightNow = Calendar.getInstance();
			hour = rightNow.get(Calendar.HOUR_OF_DAY);
			min = rightNow.get(Calendar.MINUTE);
			sec = rightNow.get(Calendar.SECOND);
			if(hour>=18||hour<6){
				
				if(sec%2==0&&min<2){
					buttonOpen.setIcon(imageIconnight3);
				}
				else if(sec%5==0){
					buttonOpen.setIcon(imageIconnight2);
				}
				else{
					buttonOpen.setIcon(imageIconnight);
				}
			}
			else{
				if(sec%2==0&&min<2){
					buttonOpen.setIcon(imageIconday3);
				}
				else if(sec%2==0){
					buttonOpen.setIcon(imageIconday2);
				}
				else{
					buttonOpen.setIcon(imageIconday);
				}
			}
			
		}

	}
	class Mainopen implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Mainproject main = new Mainproject();
			main.run(main, 450, 280);
			
			
		}
		
	}
	/**
	 * load picture from file.
	 */
	public void loadPicture(){
		awaking = loader.getResource("images/office_awaking.png");
		imageIconday = new ImageIcon(awaking);
		sleeping = loader.getResource("images/office_sleeping.png");
		imageIconnight = new ImageIcon(sleeping);
		awaking2 = loader.getResource("images/office_awaking2.png");
		imageIconday2 = new ImageIcon(awaking2);
		sleeping2 = loader.getResource("images/office_sleeping2.png");
		imageIconnight2 = new ImageIcon(sleeping2);
		awaking3 = loader.getResource("images/office_awaking_newhour.png");
		imageIconday3 = new ImageIcon(awaking3);
		sleeping3 = loader.getResource("images/office_sleepingnewhour.png");
		imageIconnight3 = new ImageIcon(sleeping3);
	}
	/**
	 * 
	 * @param frame .
	 * @param width .
	 * @param height .
	 */
	
	public void run(JFrame frame, int width, int height) {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(width, height);
		frame.setVisible(true);
	}
	/**
	 * 
	 * @param args .
	 */
	public static void main(String[] args) {
		HelperInterface helper = new HelperInterface();
		SwingUtilities.invokeLater(()->helper.run(helper, 200, 250));

	}
}