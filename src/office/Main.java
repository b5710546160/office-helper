package office;

import javax.swing.SwingUtilities;

/**
 * Main class for run project.
 * @author Kitipoom Kongpetch
 *
 */
public class Main {
	/**
	 * Run program.
	 * @param args .
	 */
	public static void main(String[] args) {
		HelperInterface helper = new HelperInterface();
		SwingUtilities.invokeLater(()->helper.run(helper, 200, 250));

	}
}
