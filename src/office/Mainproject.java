package office;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;

import office.Note.EditButtonListener;
/**
 * Main project.
 * @author Kitipoom Kongpetch
 *
 */
public class Mainproject extends JFrame {
	private JTextField timescreen, datescreen;
	private JButton note,cal,alarm,music,convert,stwatch;
	private Calendar rightNow;
	private int hour,min,sec,date,mount,year,day;
	private JLabel editor;
	private String nowday;
	/**
	 * Set layout.
	 */
	public Mainproject() {
		setTitle("Mainpage");
		setResizable(false);
		setAlwaysOnTop( true );
		timescreen = new JTextField(10);
		datescreen = new JTextField(10);
		timescreen.setBackground(Color.BLACK);
		timescreen.setEditable(false);
		timescreen.setFont(new Font("Arial", Font.PLAIN, 60));
		datescreen.setBackground(Color.red);
		datescreen.setFont(new Font("Arial", Font.PLAIN, 60));
		datescreen.setEditable(false);
		editor = new JLabel("Design by Kitipoom Kongpetch");
		timescreen.setForeground(Color.yellow);
		datescreen.setForeground(Color.BLUE);
		timescreen.setHorizontalAlignment(JTextField.CENTER);
		datescreen.setHorizontalAlignment(JTextField.CENTER);
		note = new JButton("Note");
		cal = new JButton("Calculator");
		alarm = new JButton("Alarm");
		music = new JButton("Music");
		convert = new JButton("Converter");
		stwatch= new JButton("Stopwatch");
		note.setFont(new Font("Arial", Font.PLAIN, 15));
		cal.setFont(new Font("Arial", Font.PLAIN, 15));
		alarm.setFont(new Font("Arial", Font.PLAIN, 15));
		music.setFont(new Font("Arial", Font.PLAIN, 15));
		convert.setFont(new Font("Arial", Font.PLAIN, 15));
		stwatch.setFont(new Font("Arial", Font.PLAIN, 15));
		Container cp = getContentPane();
		JPanel p = new JPanel();
		BoxLayout layout = new BoxLayout(cp, BoxLayout.Y_AXIS);
		cp.setLayout(layout);
		cp.add(datescreen);
		cp.add(timescreen);
		p.add(note);
		p.add(cal);
		p.add(alarm);
		cp.add(p);
		p = new JPanel();
		p.add(music);
		p.add(convert);
		p.add(stwatch);
		cp.add(p);
		cp.add(editor);
		Timer t = new Timer(500, new Listener());
		t.start();
		ActionListener opnote = new Noteopen();
		note.addActionListener(opnote);
		ActionListener opcal = new Calopen();
		cal.addActionListener(opcal);
		ActionListener opala = new Alarmopen();
		alarm.addActionListener(opala);
		ActionListener opmusic = new Musicopen();
		music.addActionListener(opmusic);
		ActionListener opconvert = new Convertopen();
		convert.addActionListener(opconvert);
		ActionListener opstwatch = new Stopwatchopen();
		stwatch.addActionListener(opstwatch);
		
	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			rightNow = Calendar.getInstance();
			hour = rightNow.get(Calendar.HOUR_OF_DAY);
			min = rightNow.get(Calendar.MINUTE);
			sec = rightNow.get(Calendar.SECOND);
			day =rightNow.get(Calendar.DAY_OF_WEEK);
			date =rightNow.get(Calendar.DATE); 
			mount=rightNow.get(Calendar.MONTH);
			year= rightNow.get(Calendar.YEAR);
			nowday = getDay(day);
			timescreen.setText(hour + ":" + min + ":" + sec);
			datescreen.setText(nowday+" "+date+"/"+mount+"/"+year);
		}

	}
	class Noteopen implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Note notes = new Note();
			notes.run(notes, 400, 300);
			
		}
		
	}
	class Calopen implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Calculator alarms = new Calculator();
			alarms.run(alarms,400, 300);
			
		}
		
	}
	class Alarmopen implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Alarm cals = new Alarm();
			cals.run(cals,300, 150);
			
		}
		
	}
	class Musicopen implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Musicplayer musics = new Musicplayer();
			musics.run(musics, 450, 120);
		}
		
	}
	class Convertopen implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			ConverterUI converts = new ConverterUI();
			converts.run(converts,300,150);
			
		}
		
	}
	class Stopwatchopen implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			StopwatchUI stopwatch = new StopwatchUI();
			stopwatch.run(stopwatch, 400, 300);
			
		}
		
	}
	/**
	 * Check day.
	 * @param day .
	 * @return day
	 */
	public String getDay(int day){
		if(day==1){
			return "Sun";
		}
		else if(day==2){
			return "Mon";
		}
		else if(day==3){
			return "Tue";
		}
		else if(day==4){
			return "Wed";
		}
		else if(day==5){
			return "Thu";
		}
		else if(day==6){
			return "Fri";
		}
		else if(day==7){
			return "Sat";
		}
		return null;
	}
	/**
	 * 
	 * @param args .
	 * @throws LineUnavailableException .
	 * @throws IOException .
	 */
	public static void main(String[] args) throws LineUnavailableException,
			IOException {
		run(new Mainproject(), 450, 280);
		Mainproject main = new Mainproject();
	}

	/**
	 * Run program.
	 * 
	 * @param frame
	 *            program
	 * @param width
	 *            width
	 * @param height
	 *            height
	 */
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
}
