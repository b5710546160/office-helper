package office;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class Morecalculator extends JFrame {
	private JLabel num1Label, resultLabel;
	private JTextField num1TextField, resultTextField;
	private JButton sinB, cosB, tanB, cosecB, clearB, secB,sqrtB,cotB;
	private double num1, num2;

	/**
	 * Science Calculator.
	 */
	public Morecalculator() {
		setTitle("MoreCalculator");
		setResizable(false);
		Container pane = getContentPane();
		GridLayout aGrid = new GridLayout(6, 2);
		pane.setLayout(aGrid);
		num1Label = new JLabel("1st Number");
		new JLabel("2nd Number");
		resultLabel = new JLabel("Result");

		num1TextField = new JTextField(10);
		new JTextField(10);
		resultTextField = new JTextField(10);
		resultTextField.setEditable(false);

		sinB = new JButton("sin");
		cosB = new JButton("cos");
		tanB = new JButton("tan");
		cosecB = new JButton("cosec");
		clearB = new JButton("C");
		secB = new JButton("sec");
		sqrtB =new JButton("sqrt");
		cotB=new JButton("cot");

		pane.add(num1Label);
		pane.add(num1TextField);

		pane.add(sinB);
		pane.add(cosB);
		pane.add(tanB);
		pane.add(cosecB);
		pane.add(secB);
		pane.add(sqrtB);
		pane.add(cotB);
		pane.add(clearB);
		

		pane.add(resultLabel);
		pane.add(resultTextField);

		SinButtonHandler sinbHandler = new SinButtonHandler();
		sinB.addActionListener(sinbHandler);

		CosButtonHandler cosHandler = new CosButtonHandler();
		cosB.addActionListener(cosHandler);

		TanButtonHandler tanHandler = new TanButtonHandler();
		tanB.addActionListener(tanHandler);

		SecButtonHandler secHandler = new SecButtonHandler();
		secB.addActionListener(secHandler);
		
		SqrtButtonHandler sqrtHandler = new SqrtButtonHandler();
		sqrtB.addActionListener(sqrtHandler);

		ClearButtonHandler clearHandler = new ClearButtonHandler();
		clearB.addActionListener(clearHandler);
		
		CotButtonHandler cotHandler = new CotButtonHandler();
		cotB.addActionListener(cotHandler);

		cosecB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String num1Str = num1TextField.getText();
				if (num1Str.length() > 0 ) {
					setNumber(num1Str);
					double result = 1.0/Math.sin(num1); 
					String resultStr = Double.toString(result);
					resultTextField.setText(resultStr);
				}
			}

		});

	}

	private class SinButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			if (num1Str.length() > 0) {
				setNumber(num1Str);
				double result = Math.sin(num1);
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);
			}
		}
	}

	private class CosButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			if (num1Str.length() > 0) {
				setNumber(num1Str);
				double result = Math.cos(num1);
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);
			}
		}
	}

	private class TanButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			if (num1Str.length() > 0 ) {
				setNumber(num1Str);
				double result = Math.tan(num1);
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);

			}
		}
	}

	private class SecButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			if (num1Str.length() > 0) {
				setNumber(num1Str);
				double result = 1.0/Math.sin(num1);
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);

			}
		}
	}
	private class SqrtButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String num1Str = num1TextField.getText();
			if (num1Str.length() > 0) {
				setNumber(num1Str);
				double result = Math.sqrt(num1);
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);

			}
		}
	}
	private class CotButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			if (num1Str.length() > 0) {
				setNumber(num1Str);
				double result = 1.0/Math.tan(num1);
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);

			}
		}
		
	}
	private class ClearButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			num1TextField.setText("");
			resultTextField.setText("");

		}
	}
	/**
	 * 
	 * @param num1Str Text input
	 */
	private void setNumber(String num1Str){
		num1 = Double.parseDouble(num1Str);
	}
	/**
	 * Run program.
	 * @param frame .
	 * @param width .
	 * @param height .
	 */
	
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
	/**
	 * 
	 * @param args .
	 */
	public static void main(String[] args) {
		Morecalculator aCalculatorGui = new Morecalculator();
		aCalculatorGui.run(aCalculatorGui, 250, 250);

	}
}
