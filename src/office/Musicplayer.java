package office;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;





import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.event.*;

import sun.audio.*;
import javafx.scene.media.*;
/**
 * Play music.
 * @author Kitipoom Kongpetch
 *
 */
public class Musicplayer extends JFrame {
	private JTextField filename = new JTextField(), dir = new JTextField();
	private JButton open, run, play, pause;
	private String host = null;
	private SwitchState turnstate = SwitchState.OFF;
	private AudioInputStream soundSteam = null;
	private Clip clip;
	private boolean isPlaying = false;
	private boolean isPause = false;

	/**
	 * Set Layout.
	 */
	public Musicplayer() {
		setTitle("Musicplayer");
		open = new JButton("Open");
		run = new JButton("Run");
		play = new JButton("Play");
		pause = new JButton("Pause");
		filename = new JTextField(30);
		Container cp = getContentPane();
		JPanel p = new JPanel();
		BoxLayout layout = new BoxLayout(cp, BoxLayout.Y_AXIS);
		cp.setLayout(layout);
		p.add(filename);
		p.add(open);
		cp.add(p);
		p = new JPanel();
		p.add(run);
		p.add(play);
		p.add(pause);
		cp.add(p);
		filename.setEditable(false);
		open.addActionListener(new OpenL());
		run.addActionListener(new RunL());
		play.addActionListener(new PlayL());
		pause.addActionListener(new PauseL());
		setResizable(false);
	}

	class OpenL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JFileChooser c = new JFileChooser();
			c.addChoosableFileFilter(new FiletextFilter());
			c.setAcceptAllFileFilterUsed(false);
			int rVal = c.showOpenDialog(Musicplayer.this);
			if (rVal == JFileChooser.APPROVE_OPTION) {
				filename.setText(c.getSelectedFile().getAbsolutePath());
				host = c.getSelectedFile().getAbsolutePath();
				turnstate = SwitchState.OFF;
				if(isPlaying){
					clip.stop();
				}
			}
			if (rVal == JFileChooser.CANCEL_OPTION) {
				filename.setText("You pressed cancel");
			}
		}
	}

	class RunL implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (host != null&&turnstate == SwitchState.OFF) {
				String song = filename.getText();
				
				File audioFile = new File(host);

				
				try {
					soundSteam =AudioSystem.getAudioInputStream(audioFile);
					
				} catch (UnsupportedAudioFileException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					clip = AudioSystem.getClip();
				} catch (LineUnavailableException e) {
					e.printStackTrace();
				}
				try {
					clip.open(soundSteam);
					turnstate = SwitchState.ON;
					filename.setText(host+" OK");
				} catch (LineUnavailableException | IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
	class PlayL implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(turnstate == SwitchState.ON){
				clip.loop(Clip.LOOP_CONTINUOUSLY);
				clip.start();
				isPlaying = true;
			}
			
		}	
	}
	class PauseL implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(turnstate == SwitchState.ON){
				clip.stop();
				isPlaying = false;
			}
			
		}
		
	}
	/**
	 * 
	 * @param args .
	 * @throws LineUnavailableException .
	 * @throws IOException .
	 */
	public static void main(String[] args) throws LineUnavailableException,
			IOException {
		Musicplayer mp = new Musicplayer();
		run(mp, 450, 120);
	}

	/**
	 * Run program.
	 * 
	 * @param frame
	 *            program
	 * @param width
	 *            width
	 * @param height
	 *            height
	 */
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
}
