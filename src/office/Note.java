package office;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Note for write.
 * @author Kitipoom Kongpetch
 *
 */
public class Note extends JFrame {
	private JTextArea spacenote;
	private JScrollPane pane;
	private JButton lock, unlock,night;
	private SwitchState nightmode = SwitchState.OFF; 
	
	/**
	 * set layout.
	 */
	public Note() {
		setTitle("Note");
		spacenote = new JTextArea(100, 40);
		lock = new JButton("Lock");
		unlock = new JButton("Edit");
		night = new JButton("Light");
		spacenote.setFont(new Font("ansna new", Font.PLAIN, 20));
		pane = new JScrollPane(spacenote);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		Container cp = getContentPane();
		JPanel p = new JPanel();
		BoxLayout layout = new BoxLayout(cp, BoxLayout.Y_AXIS);
		cp.setLayout(layout);
		cp.add(pane);
		p = new JPanel();
		p.add(night);
		p.add(lock);
		p.add(unlock);
		cp.add(p);
		ActionListener lockA = new LockButtonListener();
		lock.addActionListener(lockA);
		ActionListener editA = new EditButtonListener();
		unlock.addActionListener(editA);
		ActionListener nightA = new NightButtonListener();
		night.addActionListener(nightA);
	}
	class LockButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			spacenote.setEditable(false);
			
		}
		
	}
	class EditButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			spacenote.setEditable(true);
			
		}
		
	}
	class NightButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(nightmode == SwitchState.OFF){
				nightmode = SwitchState.ON;
				night.setText("Night");
				spacenote.setBackground(Color.BLACK);
				spacenote.setForeground(Color.WHITE);
			}
			else{
				nightmode = SwitchState.OFF;
				night.setText("Light");
				spacenote.setBackground(Color.WHITE);
				spacenote.setForeground(Color.BLACK);
			}
		}
		
	}
	/**
	 * 
	 * @param args .
	 * @throws LineUnavailableException .
	 * @throws IOException .
	 */
	public static void main(String[] args) throws LineUnavailableException,
			IOException {
		run(new Note(), 400, 300);
		Note note = new Note();
	}

	/**
	 * Run program.
	 * 
	 * @param frame
	 *            program
	 * @param width
	 *            width
	 * @param height
	 *            height
	 */
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
}
