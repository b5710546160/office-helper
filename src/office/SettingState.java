package office;
/**
 * Time setting state.
 * @author Kitipoom Kongpetch
 *
 */
public enum SettingState {
	HOUR,
	MIN,
	SEC,
	NON;
	
}
