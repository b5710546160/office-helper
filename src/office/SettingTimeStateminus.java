package office;
/**
 * Minus time.
 * @author Kitipoom Kongpetch
 *
 */
public class SettingTimeStateminus extends ClockState{
	private String normaltime;
	@Override
	public int performSet(int time, SettingState state) {
		if(state==SettingState.HOUR){
			time--;
			if(time==-1){
				time=23;
			}
		}
		else if(state==SettingState.MIN||state==SettingState.SEC){
			time--;
			if(time==-1){
				time=59;
			}
		}
		return time;
	}

	@Override
	public String updateTime(int hour, int min, int sec) {
		normaltime= String.format("%2d:%2d:%2d", hour, min, sec);
		return normaltime;
	}

}
