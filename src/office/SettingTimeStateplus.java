package office;
/**
 * Plus time.
 * @author Kitipoom Kongpetch
 *
 */
public class SettingTimeStateplus extends ClockState{
	private String normaltime;
	@Override
	public int performSet(int time, SettingState state) {
		if(state==SettingState.HOUR){
			time++;
			if(time==24){
				time=0;
			}
		}
		else if(state==SettingState.MIN||state==SettingState.SEC){
			time++;
			if(time==60){
				time=0;
			}
		}
		return time;
	}

	@Override
	public String updateTime(int hour, int min, int sec) {
		normaltime= String.format("%2d:%2d:%2d", hour, min, sec);
		return normaltime;
	}

}
