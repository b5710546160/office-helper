package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */

public class StopWatch {
	/** constant for converting nanoseconds to seconds. */
	private static final double NANOSECONDS = 1.0E-9;
	/** time that the stopwatch was started, in nanoseconds. */
	private long startTime = 0;
	/** time that the stopwatch was stoped, in nanoseconds. */
	private long stopTime = 0;
	private double elapsed =0;
	private boolean running = false;
	/**
	 * Stop watch.
	 */
	public StopWatch (){
		startTime = System.nanoTime();
	}
	/**
	 * Show time now.
	 * @return double
	 */
	public double getElapsed( ) {
		
		if(running){
			this.elapsed=this.NANOSECONDS*(System.nanoTime()-this.startTime);
			
		
		}
		else{
			this.elapsed= this.NANOSECONDS*(this.stopTime-this.startTime);
		}
		return this.elapsed;
	}
	/**
	 * Check time stop or not.
	 * @return boolean
	 */
	public boolean isRunning( ) {
		return running ? true : false;
		
		
	}
	/**
	 * Start.
	 */
	public void start(){
		if(isRunning( )){
			
		}
		else{
			this.running= true;
			this.startTime= System.nanoTime();
		}
		
	}
	/**
	 * Stop.
	 */
    public void stop(){
		if(running){
			this.running= false;
			this.stopTime= System.nanoTime();
		}
		else{
			
		}
	}
}

