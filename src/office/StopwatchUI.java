package office;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;

/**
 * Stopwatch.
 * @author Kitipoom Kongpetch
 *
 */
public class StopwatchUI extends JFrame {
	private StopWatch watch = new StopWatch();
	private Timecontrol t = new Timecontrol();
	private JButton start, stop,record;
	private String timenow="0:0:0";
	private String recordT="";
	private JTextArea timerecord;
	private JTextField time;
	private JMenu menu;
	private JMenuBar menubar;
	private JScrollPane pane;
	private String control = "White";
	/**
	 * Set JFrame.
	 */
	public StopwatchUI(){
		this.setTitle("Stopwatch");
		setResizable(false);
		timerecord = new JTextArea(10, 40);
		timerecord.setFont(new Font("Arial", Font.PLAIN, 20));
		timerecord.setEditable(false);
		pane = new JScrollPane(timerecord);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		time = new JTextField(5);
		time.setFont(new Font("Arial", Font.PLAIN, 60));
		time.setHorizontalAlignment(JTextField.CENTER);
		record = new JButton("Record");
		start = new JButton("Start");
		stop = new JButton("Stop");
		menubar = new JMenuBar();
		menu = new JMenu("Color");
		Colortype[] ct = Colortype.values();
		for (int i = 0; i < ct.length; i++) {
			JMenuItem item = new JMenuItem(ct[i].toString());
			item.addActionListener(new MenuActionListener());
			menu.add(item);
		}
		menu.getSelectedIcon();
		menubar.add(menu);
		Container cp = getContentPane();
		JPanel p = new JPanel();
		BoxLayout layout = new BoxLayout(cp, BoxLayout.Y_AXIS);
		cp.setLayout(layout);
		p.add(time);
		cp.add(p);
		p = new JPanel();
		p.add(menubar);
		p.add(start);
		p.add(stop);
		p.add(record);
		cp.add(p);
		cp.add(pane);
		Timer t = new Timer(50, new Listener());
		t.start();
		ActionListener startaction = new StartButtonListener();
		start.addActionListener(startaction);
		ActionListener stopaction = new StopButtonListener();
		stop.addActionListener(stopaction);
		ActionListener recordaction = new CaptureButtonListener();
		record.addActionListener(recordaction);
	}
	class Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(watch.isRunning()){
				timenow =t.controltimer(watch.getElapsed()); 
				time.setText(timenow);
			}
			else{
				time.setText(timenow);
			}
		}
	}
	class StartButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(watch.isRunning()){
				
			}
			else{
				watch.start();
				timerecord.setText("");
				recordT="";
			}
		}
		
	}
	class StopButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			watch.stop();
			
		}
		
	}
	class CaptureButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			recordT+= String.format("%s\n", timenow);
			timerecord.setText(recordT);
			
			
		}
		
	}
	class MenuActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			control = e.getActionCommand();
			if (e.getActionCommand().equals("Red")) {
				time.setBackground(Color.red);
				time.setForeground(Color.blue);
				
			} else if (e.getActionCommand().equals("Blue")) {
				time.setBackground(Color.BLUE);
				time.setForeground(Color.RED);
			} else if (e.getActionCommand().equals("Green")) {
				time.setBackground(Color.green);
				time.setForeground(Color.YELLOW);
			} else if (e.getActionCommand().equals("Black")){
				time.setBackground(Color.BLACK);
				time.setForeground(Color.WHITE);
			} 
			else if(e.getActionCommand().equals("White")){
				time.setBackground(Color.WHITE);
				time.setForeground(Color.BLACK);
			}
		}

	}
	/**
	 * 
	 * @param args .
	 * @throws LineUnavailableException .
	 * @throws IOException .
	 */
	public static void main(String[] args) throws LineUnavailableException,
			IOException {
		StopwatchUI note = new StopwatchUI();
		run(note, 400, 300);
		
	}

	/**
	 * Run program.
	 * 
	 * @param frame
	 *            program
	 * @param width
	 *            width
	 * @param height
	 *            height
	 */
	public static void run(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setVisible(true);
	}
}
