package office;
/**
 * SwitchState for turn on-off.
 * @author Kitipoom Kongpetch
 *
 */
public enum SwitchState {
	ON,
	OFF;
}
