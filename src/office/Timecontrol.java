package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class Timecontrol {
	private int min=0;
	private int sec=0;
	private int minsec=0;
	private String timenow;
	/**
	 * 
	 * @param time beforechange
	 * @return String currenttime
	 */
	public String controltimer(double time){
		minsec = (int) (time%1*100);
		sec = (int) (time/1);
		if(sec>=60){
			min = sec/60;
			sec=sec%60;
		}
		return min+":"+sec+":"+minsec;
		
	}
}
