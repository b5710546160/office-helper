package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class UnitConverter {
	/**
	 * 
	 */
	public UnitConverter() {

	}

	/**
	 * 
	 * @param amount
	 *            value we want to convert
	 * @param fromUnit
	 *            Unitbase
	 * @param toUnit
	 *            target Unit
	 * @return value is convert
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit) {
		return fromUnit.convertTo(toUnit, amount);

	}

	/**
	 * @param c name of Unit.
	 * @return All type unit
	 */
	public Unit[] getUnits(String c) {
		Unit[] a;
		if (c.equals("Length")) {
			a = Length.values();
		} else if (c.equals("Area")) {
			a = Area.values();
		} else if (c.equals("Weight")) {
			a = Weight.values();
		} else if (c.equals("Cube")) {
			a = Cube.values();
		} else {
			a = null;
		}

		return a;

	}
}
