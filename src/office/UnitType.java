package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public enum UnitType {
	Length("Length"), Area("Area"), Weight("Weight"), 
	Cube("Cube"); // your choice
	public final String name;
	private UnitType(String name) {
		this.name=name;
	}
	/**
	 * @return name of Unittype.
	 */
	public String toString() {
		return this.name;
	}
}
