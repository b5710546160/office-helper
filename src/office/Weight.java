package office;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public enum Weight implements Unit {
	GRAM("gram", 1.0), CENTRIGRAM("centrigram", 0.01), KILOGRAM("kilogram",
			1000.0), TON("Ton", 1000000), QUINTAL("quintal", 100000), MYRIAGRAM(
			"myriagram", 10000), HECTOGRAM("hectogram", 100), DEKAGRAM(
			"dekagram", 10), DECIGRAM("decigram", 0.1), MILLIGRAMS(
			"milligrams", 0.001);
	public final double value;
	public final String name;

	private Weight(String name, double value) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 
	 * @param a
	 *            name of type
	 * @return Unit
	 */
	public Weight check(String a) {
		if (a.equals("gram")) {
			return Weight.GRAM;
		}
		if (a.equals("centrigram")) {
			return Weight.CENTRIGRAM;
		}
		if (a.equals("kilogram")) {
			return Weight.KILOGRAM;
		}
		if (a.equals("Ton")) {
			return Weight.TON;
		}
		if (a.equals("myriagram")) {
			return Weight.MYRIAGRAM;
		}
		if (a.equals("hectogram")) {
			return Weight.HECTOGRAM;
		}
		if (a.equals("dekagram")) {
			return Weight.DEKAGRAM;
		}
		if (a.equals("decigram")) {
			return Weight.DECIGRAM;
		}
		if (a.equals("milligrams")) {
			return Weight.MILLIGRAMS;
		}
		return null;
	}

	@Override
	public double convertTo(Unit u, double amt) {

		Weight base = check(this.name);
		Weight sub = check(u.toString());
		double result = amt * base.getValue() / sub.getValue();
		return result;
	}

	@Override
	public double getValue() {

		return this.value;
	}

	@Override
	public String toString() {
		return this.name;

	}

}
